const {ApolloServer, gql} = require('apollo-server');
const db = require('./src/dao/db');
const mutations = require('./src/resolvers/mutations');
const queries = require('./src/resolvers/queries');

// The GraphQL schema
const typeDefs = gql(`
  type Query {
    user(Id: Int!): Person
    users: [Person]
    tasks(User: Int!, Offset: Int, Count: Int): [Task]
  },
  type Mutation{
    createUser(Name:String!, Surname: String!, Notes: String): Person
    createTask(CreatedBy: Int!, Status: Status!, Items: [TaskItemInput], Assignee: Int, DueDate: String, Notes: String): Task
    createTaskItem(User:Int!, TaskId: Int!, Item: TaskItemInput) : Task
    deleteTaskItem(User:Int!, TaskId: Int!, ItemId: Int!) : Task
    updateTaskItemStatus(User:Int!, TaskId: Int!, ItemId: Int!, Status: Status) : Task
    updateTask(User:Int!, TaskId: Int!, Assignee:Int, Status: Status) : Task
    unAssignTask(User:Int!, TaskId: Int!) : Task
  }
  type Person {
    Id: Int
    Name: String
    Surname: String
    Notes: String
  },
  type Task {
    Id: Int
    CreatedBy: Int
    Status: Status
    Items: [TaskItem]
    Assignee: Int
    DueDate: String
    Notes: String
  },
  
  type TaskItem {
    Id: Int
    Description: String
    Status: Status
  }
  input TaskItemInput {
    Description: String!
    Status: Status!
  }
  enum Status {
    Open
    Completed
  }
`);

const resolvers = {
	Query: queries,
	Mutation: mutations
};

const server = new ApolloServer({
	typeDefs,
	resolvers,
});

const init = async () => {
	await db.init();
	const {url} = await server.listen();
	console.log(`🚀 Server ready at ${url}`);
};

init();
