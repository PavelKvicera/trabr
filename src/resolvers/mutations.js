'use strict';
const db = require('../dao/db');
const Boom = require('boom');

exports.createUser = (parent, user) => {
	return db.insertUser(user);
};

exports.createTask = (parent, task) => {
	if (db.getUserById(task.CreatedBy) && db.getUserById(task.Assignee)) {
		return db.insertTask(task);
	} else {
		throw Boom.notFound('user not found, only existing user can create task or be assigned to task');
	}
};

exports.createTaskItem = (parent, {User, TaskId, Item}) => {
	const task = db.getTaskById(TaskId);
	if (User === task.CreatedBy || User === task.Assignee) {
		const newItems = [...task.Items];
		const lastItem = (task.Items.slice(-1))[0] || {};
		Item.Id = lastItem.Id + 1 || 0;
		newItems.push(Item);
		task.Items = newItems;
		db.updateTask(TaskId, task);
		return task;
	} else {
		throw Boom.forbidden('user does not have access to this task');
	}
};

exports.deleteTaskItem = (parent, {User, TaskId, ItemId}) => {
	const task = db.getTaskById(TaskId);
	if (User === task.CreatedBy || User === task.Assignee) {
		const newItems = task.Items.filter(item => {
			return item.Id !== ItemId;
		});
		task.Items = newItems;
		db.updateTask(TaskId, task);
		return task;
	} else {
		throw Boom.forbidden('user does not have access to this task');
	}
};

exports.updateTaskItemStatus = (parent, {User, TaskId, ItemId, Status}) => {
	const task = db.getTaskById(TaskId);
	if (User === task.CreatedBy || User === task.Assignee) {
		const newItems = task.Items.map(item => {
			return item.Id !== ItemId ? item : {Id: item.Id, Description: item.Description, Status: Status};
		});
		task.Items = newItems;
		db.updateTask(TaskId, task);
		return task;
	} else {
		throw Boom.forbidden('user does not have access to this task');
	}
};

exports.updateTask = (parent, {User, TaskId, Status, Assignee}) => {
	const task = db.getTaskById(TaskId);
	if (User === task.CreatedBy || User === task.Assignee) {
		if (Status === 'Open') {
			task.Status = Status;
		} else if (Status === 'Completed') {
			if (task.Items.some(item => item.Status === 'Open')) {
				return Boom.conflict('all task items must be completed first');
			}
			task.Status = Status;
		}
		if (Assignee) {
			if (!db.getUserById(Assignee)) return Boom.notFound('Assignee user does not exist')
			task.Assignee = Assignee;
		};
		db.updateTask(TaskId, task);
		return task;
	} else {
		throw Boom.forbidden('user does not have access to this task');
	}
};

exports.unAssignTask = (parent, {User,TaskId}) => {
	const task = db.getTaskById(TaskId);
	if (User === task.CreatedBy || User === task.Assignee) {
		task.Assignee = undefined;
		db.updateTask(TaskId, task);
		return task;
	} else {
		throw Boom.forbidden('user does not have access to this task');
	}
}