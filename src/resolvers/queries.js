'use strict';
const db = require('../dao/db');
exports.users = () => {
	return db.getAllUsers();
};

exports.user = (parent, {Id}) => {
	return db.getUserById(Id);
}

exports.tasks = (parent,{User,Offset,Count}) => {
	return db.getTaskByCreatorOrAssignee(User, Offset, Count)
}