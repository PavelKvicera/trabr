'use strict';
const low = require('lowdb');
const FileAsync = require('lowdb/adapters/FileAsync');
const adapter = new FileAsync('./db/db.json');

class db {
	async init() {
		return low(adapter).then(lowdb => {
			this.lowdb = lowdb;
			console.log('Db ready');
		});
	}

	generateUserId() {
		const id = this.lowdb.get('users')
			.size()
			.value();
		return id + 1;
	}

	generateTaskId() {
		const id = this.lowdb.get('tasks')
			.size()
			.value();
		return id + 1;
	}

	async insertUser(user) {
		user.Id = await this.generateUserId();
		return this.lowdb.get('users')
			.push(user)
			.write()
			.then(() => {
				return user;
			});
	}

	getAllUsers() {
		return this.lowdb.get('users');
	}

	getUserById(id) {
		return this.lowdb.get('users')
			.find({Id: id})
			.value();
	}

	async insertTask(task) {
		task.Id = await this.generateTaskId();
		task.Items = task.Items.map((item, id) => {
			return {Id: id, Description: item.Description, Status: item.Status};
		});
		return this.lowdb.get('tasks')
			.push(task)
			.write()
			.then(() => {
				return task;
			});
	}

	getTaskByCreatorOrAssignee(user,offset = 0, count) {
		const tasks = this.lowdb.get('tasks')
			.sortBy('Id')
			.value();
		const end =  count ? offset+count : undefined
		return tasks.filter(task => {return task.CreatedBy === user || task.Assignee === user} ).slice(offset,end)
	}

	getTaskById(id) {
		return this.lowdb.get('tasks')
			.find({Id:id})
			.value()
	}

	updateTask(id,task) {
		return this.lowdb.get('tasks')
			.find({Id:id})
			.assign(task)
			.write()
			.then()
	}

}

module.exports = new db();