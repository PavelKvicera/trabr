# trabr Backend Engineer Tech Test

## Installation
 - clone repo 
 - run npm install in root of repo
 - run npm start to start server
 - server is accesible on port 4000
 - live version is accessible on http://development.hotdash.net:4000/
 
## Notes
 - user authentication was not required hence user id needs to be supplied in payload in order to determine user access to resource, this would normally be pulled from auth object (JWT, etc ..)
 - lowdb has been used for data mockup
 

 